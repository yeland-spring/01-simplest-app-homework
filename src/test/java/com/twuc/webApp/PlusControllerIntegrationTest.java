package com.twuc.webApp;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@AutoConfigureMockMvc
class PlusControllerIntegrationTest {
    @Autowired
    private MockMvc mockMvc;

    @Test
    void should_return_plus_table_when_test() throws Exception {
        String expect = "1+1=2  \n" +
                "2+1=3  2+2=4  \n" +
                "3+1=4  3+2=5  3+3=6  \n" +
                "4+1=5  4+2=6  4+3=7  4+4=8  \n" +
                "5+1=6  5+2=7  5+3=8  5+4=9  5+5=10 \n" +
                "6+1=7  6+2=8  6+3=9  6+4=10 6+5=11 6+6=12 \n" +
                "7+1=8  7+2=9  7+3=10 7+4=11 7+5=12 7+6=13 7+7=14 \n" +
                "8+1=9  8+2=10 8+3=11 8+4=12 8+5=13 8+6=14 8+7=15 8+8=16 \n" +
                "9+1=10 9+2=11 9+3=12 9+4=13 9+5=14 9+6=15 9+7=16 9+8=17 9+9=18 \n";
        mockMvc.perform(MockMvcRequestBuilders.get("/api/tables/plus"))
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.content().string(expect));
    }
}