package com.twuc.webApp;

public class GetTable {
    public String getFormula(int left, int right, String operator) {
        int result, maxResult;
        result = (operator.equals("+")) ? left + right : left * right;
        maxResult = (operator.equals("+")) ? 9 + right : 9 * right;
        String format = String.format("%d%s%d=%d", left, operator, right, result);
        if (result <= 9 && maxResult > 9) {
            return format + "  ";
        }
        return format + " ";
    }

    public String getTable(String operator) {
        StringBuilder result = new StringBuilder();
        for (int i = 1; i <= 9; i++) {
            for (int j = 1; j <= i; j++) {
                result.append(getFormula(i, j, operator));
            }
            result.append("\n");
        }
        return result.toString();
    }
}
