package com.twuc.webApp;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class MultiplyController {
    @GetMapping(value = "/api/tables/multiply")
    public String getTable() {
        GetTable getTable = new GetTable();
        return getTable.getTable("*");
    }
}
